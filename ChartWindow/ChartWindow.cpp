#include "ChartWindow.h"
#include "FourierTransformExample.h"

ChartWindow::ChartWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.pushButton, &QPushButton::clicked, this, &ChartWindow::clicked);
}

void ChartWindow::clicked()
{
	FourierTransformExample::runCalculation();
}
