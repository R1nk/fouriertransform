#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ChartWindow.h"

class ChartWindow : public QMainWindow
{
	Q_OBJECT

public:
	ChartWindow(QWidget *parent = Q_NULLPTR);

private:
	Ui::ChartWindowClass ui;

	void clicked();
};
