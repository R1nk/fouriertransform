#pragma once
#include "DataModel.h"

class OutputAdapter
{
public:
	OutputAdapter() {};
	
	void saveToCsv(const std::string& fileName, const DataModel& model);
};

