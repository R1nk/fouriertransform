#include "FourierTransformExample.h"
#include "CalculationAdapter.h"
#include "OutputAdapter.h"
#include "BaseFourierTransform.h"
#include "BaseLine.h"
#include "Stepwise.h"
#include "BaseLineNoise.h"

void FourierTransformExample::runCalculation()
{
	const size_t dataSize = 10000;
	CalculationAdapter calc(dataSize);
	OutputAdapter saveAdapter;

	std::vector<ITransform*> transformList
	{
		new BaseFourierTransform()
	};

	std::vector<IDataGenerator*> dataList
	{
		new BaseLine(1),
		new Stepwise(100, 1000),
		new BaseLineNoise(dataSize / 2, 1)
	};

	for (const auto& transform : transformList)
	{
		for (const auto& data : dataList)
		{
			const std::string fileName = transform->print() + "_" + data->print() + ".csv";
			saveAdapter.saveToCsv(fileName, calc.calculate(*transform, *data));
		}
	}
}
