#pragma once

#include <vector>
#include "IPrintable.h"

class ITransform : public IPrintable
{
public:
	virtual ~ITransform() {};
	virtual std::vector<double> transform(const std::vector<double>& data) const = 0;
};
