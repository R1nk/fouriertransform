#pragma once
#include <vector>
#include "IPrintable.h"

class IDataGenerator : public IPrintable
{
public:
	virtual ~IDataGenerator() {};
	virtual std::vector<double> generateFunctionData(const size_t size) const = 0;
};

