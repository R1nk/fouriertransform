#include "BaseLine.h"

std::vector<double> BaseLine::generateFunctionData(const size_t size) const
{
	return std::vector<double>(size, x);
}

std::string BaseLine::print() const
{
	return "BaseLine_" + std::to_string(x);
}
