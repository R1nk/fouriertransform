#pragma once

#include "ITransform.h"
#include <vector>

class BaseFourierTransform : public ITransform
{
	
public:
	BaseFourierTransform() = default;

	virtual std::vector<double> transform(const std::vector<double>& data) const override;
	virtual std::string print() const override;
};
