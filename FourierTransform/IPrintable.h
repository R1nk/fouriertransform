#pragma once
#include <string>

class IPrintable
{
public:
	virtual std::string print() const = 0;
};

