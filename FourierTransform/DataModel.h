#pragma once
#include <vector>
#include <string>
#include <iostream>
class DataModel
{
	std::string name;
	std::vector<double> original;
	std::vector<double> transformed;

public:
	DataModel(const std::vector<double>& orig, const std::vector<double>& tform)
		:original(orig), transformed(tform)
	{}

	friend std::ostream& operator<< (std::ostream& os, const DataModel& model);
};

