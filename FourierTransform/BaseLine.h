#pragma once

#include "IDataGenerator.h"

class BaseLine : public IDataGenerator
{
	double x;
public:
	BaseLine() = default;
	BaseLine(double a) : x(a) {};
	
	virtual std::vector<double> generateFunctionData(const size_t size) const override;
	virtual std::string print() const override;
};
