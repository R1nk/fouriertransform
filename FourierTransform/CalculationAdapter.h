#pragma once
#include "ICalculation.h"

class CalculationAdapter : public ICalculation
{
	size_t size;
public:
	CalculationAdapter(size_t s) :size(s) {};

	virtual DataModel calculate(const ITransform& pred, const IDataGenerator& data) const override;
};

