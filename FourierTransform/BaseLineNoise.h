#pragma once

#include "BaseLine.h"

class BaseLineNoise : public BaseLine
{
	int index;
public:
	BaseLineNoise(int i, double value) :BaseLine(value) { index = i; };

	virtual std::vector<double> generateFunctionData(const size_t size) const override;
	virtual std::string print() const override;
};

