#include "OutputAdapter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

void OutputAdapter::saveToCsv(const std::string& fileName, const DataModel& model)
{
	std::ofstream myfile;
	myfile.open(fileName);
	myfile << std::fixed;
	myfile << std::setprecision(10);
	myfile << model;
	myfile.close();
}
