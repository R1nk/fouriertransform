#pragma once

#include "BaseLine.h"

class Stepwise : public BaseLine
{
	double topValue;
	double bottomValue;
public :
	Stepwise(double t, double b) : topValue(t), bottomValue(b) { };

	virtual std::vector<double> generateFunctionData(const size_t size) const override;
	virtual std::string print() const override;
};

