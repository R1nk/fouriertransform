#define _USE_MATH_DEFINES
#include <math.h>
#include <complex>
#include "BaseFourierTransform.h"


std::vector<double> BaseFourierTransform::transform(const std::vector<double>& data) const
{
    const auto size = data.size();
    std::vector<double> result;
    result.reserve(size);

    for (auto k = 0; k < size; k++)
    {
        double sum = 0;
        for (auto n = 0; n < size; n++)
        {
            const auto angle = 2 * M_PI * k * n / size;
            sum += data[n] * cos(angle) / size;
        }
        result.push_back(sum);
    }

    return result;
}

std::string BaseFourierTransform::print() const
{
    return "BaseFourierTransform";
}

