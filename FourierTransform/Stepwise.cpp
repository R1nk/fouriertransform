#include "Stepwise.h"

std::vector<double> Stepwise::generateFunctionData(const size_t size) const
{
	std::vector<double> result;
	result.reserve(size);
	const auto step = (bottomValue - topValue) / size;
	double value = 0;
	for (int i = 0; i < size; i++)
	{
		value += step;
		result.push_back(value);
	}
	return result;
}

std::string Stepwise::print() const
{
	return "Stepwise";
}
