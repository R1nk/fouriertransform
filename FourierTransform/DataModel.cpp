#include "DataModel.h"

std::ostream& operator<<(std::ostream& os, const DataModel& model)
{
	for (int i = 0; i < model.original.size(); i++)
	{
		os << model.original[i] << ";" << model.transformed[i] << std::endl;
	}
	return os;
}
