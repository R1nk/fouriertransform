#include "CalculationAdapter.h"

DataModel CalculationAdapter::calculate(const ITransform& pred, const IDataGenerator& data) const
{
	const auto original = data.generateFunctionData(size);
	return DataModel(original, pred.transform(original));
}
