#include "BaseLineNoise.h"

std::vector<double> BaseLineNoise::generateFunctionData(const size_t size) const
{
	auto result = BaseLine::generateFunctionData(size);
	result[index] = result[index] + .5;
	return result;
}

std::string BaseLineNoise::print() const
{
	return "BaseLineNoise";
}
