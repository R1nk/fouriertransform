#pragma once
#include "DataModel.h"
#include "IDataGenerator.h"
#include "ITransform.h"

class ICalculation
{
public:
	virtual DataModel calculate(const ITransform& pred, const IDataGenerator& data) const = 0;
};

